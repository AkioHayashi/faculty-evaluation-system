﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FacultyManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvFaculty = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Text_SearchFM = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Text_FnameFM = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Text_LnameFM = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Text_EvaluationScoreFM = New System.Windows.Forms.TextBox()
        Me.Text_SemesterFM = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Text_EvaluationDateFM = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtIDnum = New System.Windows.Forms.TextBox()
        Me.Back_FM = New System.Windows.Forms.Button()
        CType(Me.dgvFaculty, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvFaculty
        '
        Me.dgvFaculty.AllowUserToAddRows = False
        Me.dgvFaculty.AllowUserToDeleteRows = False
        Me.dgvFaculty.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvFaculty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFaculty.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7})
        Me.dgvFaculty.Location = New System.Drawing.Point(12, 36)
        Me.dgvFaculty.Name = "dgvFaculty"
        Me.dgvFaculty.ReadOnly = True
        Me.dgvFaculty.Size = New System.Drawing.Size(669, 349)
        Me.dgvFaculty.TabIndex = 13
        '
        'ID
        '
        Me.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ID.Visible = False
        '
        'Column1
        '
        Me.Column1.HeaderText = "Last Name"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column1.Width = 110
        '
        'Column2
        '
        Me.Column2.HeaderText = "First Name"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 110
        '
        'Column3
        '
        Me.Column3.HeaderText = "Evaltuation Score"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.HeaderText = "Evaluation Date"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.HeaderText = "Semester"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.HeaderText = "Evaluated"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        '
        'Column7
        '
        Me.Column7.HeaderText = "ID Number"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Text_SearchFM
        '
        Me.Text_SearchFM.Location = New System.Drawing.Point(111, 11)
        Me.Text_SearchFM.Name = "Text_SearchFM"
        Me.Text_SearchFM.Size = New System.Drawing.Size(570, 20)
        Me.Text_SearchFM.TabIndex = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(61, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Search:"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(434, 472)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(86, 75)
        Me.btnClear.TabIndex = 23
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(56, 453)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Evaluation Score:"
        '
        'Text_FnameFM
        '
        Me.Text_FnameFM.Location = New System.Drawing.Point(122, 420)
        Me.Text_FnameFM.Name = "Text_FnameFM"
        Me.Text_FnameFM.Size = New System.Drawing.Size(276, 20)
        Me.Text_FnameFM.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 427)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "First Name:"
        '
        'Text_LnameFM
        '
        Me.Text_LnameFM.Location = New System.Drawing.Point(122, 391)
        Me.Text_LnameFM.Name = "Text_LnameFM"
        Me.Text_LnameFM.Size = New System.Drawing.Size(276, 20)
        Me.Text_LnameFM.TabIndex = 18
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(57, 398)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Last Name:"
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(526, 391)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(86, 75)
        Me.btnEdit.TabIndex = 16
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(526, 472)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(86, 75)
        Me.btnDelete.TabIndex = 15
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(434, 392)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(86, 75)
        Me.btnAdd.TabIndex = 14
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Text_EvaluationScoreFM
        '
        Me.Text_EvaluationScoreFM.Location = New System.Drawing.Point(153, 446)
        Me.Text_EvaluationScoreFM.Name = "Text_EvaluationScoreFM"
        Me.Text_EvaluationScoreFM.Size = New System.Drawing.Size(244, 20)
        Me.Text_EvaluationScoreFM.TabIndex = 24
        '
        'Text_SemesterFM
        '
        Me.Text_SemesterFM.Location = New System.Drawing.Point(118, 498)
        Me.Text_SemesterFM.Name = "Text_SemesterFM"
        Me.Text_SemesterFM.Size = New System.Drawing.Size(279, 20)
        Me.Text_SemesterFM.TabIndex = 26
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(58, 479)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Evaluation Date:"
        '
        'Text_EvaluationDateFM
        '
        Me.Text_EvaluationDateFM.Location = New System.Drawing.Point(150, 472)
        Me.Text_EvaluationDateFM.Name = "Text_EvaluationDateFM"
        Me.Text_EvaluationDateFM.Size = New System.Drawing.Size(247, 20)
        Me.Text_EvaluationDateFM.TabIndex = 28
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(58, 505)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 13)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Semester:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(58, 528)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "ID Number:"
        '
        'txtIDnum
        '
        Me.txtIDnum.Location = New System.Drawing.Point(118, 524)
        Me.txtIDnum.Name = "txtIDnum"
        Me.txtIDnum.Size = New System.Drawing.Size(279, 20)
        Me.txtIDnum.TabIndex = 31
        '
        'Back_FM
        '
        Me.Back_FM.Location = New System.Drawing.Point(12, 8)
        Me.Back_FM.Name = "Back_FM"
        Me.Back_FM.Size = New System.Drawing.Size(45, 25)
        Me.Back_FM.TabIndex = 32
        Me.Back_FM.Text = "<<"
        Me.Back_FM.UseVisualStyleBackColor = True
        '
        'FacultyManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(694, 562)
        Me.Controls.Add(Me.Back_FM)
        Me.Controls.Add(Me.txtIDnum)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Text_EvaluationDateFM)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Text_SemesterFM)
        Me.Controls.Add(Me.Text_EvaluationScoreFM)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Text_FnameFM)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Text_LnameFM)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.dgvFaculty)
        Me.Controls.Add(Me.Text_SearchFM)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FacultyManagementForm"
        Me.Text = "Faculty Management Form"
        CType(Me.dgvFaculty, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvFaculty As System.Windows.Forms.DataGridView
    Friend WithEvents Text_SearchFM As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Text_FnameFM As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Text_LnameFM As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents Text_EvaluationScoreFM As System.Windows.Forms.TextBox
    Friend WithEvents Text_SemesterFM As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Text_EvaluationDateFM As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtIDnum As System.Windows.Forms.TextBox
    Friend WithEvents Back_FM As System.Windows.Forms.Button
End Class
