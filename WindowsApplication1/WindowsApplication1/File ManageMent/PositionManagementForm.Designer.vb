﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PositionManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PositionManagementForm))
        Me.Dgv_Position = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Clear_Position = New System.Windows.Forms.Button()
        Me.Edit_Position = New System.Windows.Forms.Button()
        Me.Delete_Position = New System.Windows.Forms.Button()
        Me.Add_Position = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Text_IDPosition = New System.Windows.Forms.TextBox()
        Me.Text_Position = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Back_PositionMngmnt = New System.Windows.Forms.Button()
        CType(Me.Dgv_Position, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Dgv_Position
        '
        Me.Dgv_Position.AllowUserToAddRows = False
        Me.Dgv_Position.AllowUserToDeleteRows = False
        Me.Dgv_Position.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.Dgv_Position.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgv_Position.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.Dgv_Position.Location = New System.Drawing.Point(12, 36)
        Me.Dgv_Position.Name = "Dgv_Position"
        Me.Dgv_Position.ReadOnly = True
        Me.Dgv_Position.Size = New System.Drawing.Size(330, 271)
        Me.Dgv_Position.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "ID"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Column2
        '
        Me.Column2.HeaderText = "Position"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Column2.Width = 287
        '
        'Clear_Position
        '
        Me.Clear_Position.Location = New System.Drawing.Point(189, 357)
        Me.Clear_Position.Name = "Clear_Position"
        Me.Clear_Position.Size = New System.Drawing.Size(50, 25)
        Me.Clear_Position.TabIndex = 17
        Me.Clear_Position.Text = "Clear"
        Me.Clear_Position.UseVisualStyleBackColor = True
        '
        'Edit_Position
        '
        Me.Edit_Position.Location = New System.Drawing.Point(245, 322)
        Me.Edit_Position.Name = "Edit_Position"
        Me.Edit_Position.Size = New System.Drawing.Size(50, 25)
        Me.Edit_Position.TabIndex = 16
        Me.Edit_Position.Text = "Edit"
        Me.Edit_Position.UseVisualStyleBackColor = True
        '
        'Delete_Position
        '
        Me.Delete_Position.Location = New System.Drawing.Point(245, 357)
        Me.Delete_Position.Name = "Delete_Position"
        Me.Delete_Position.Size = New System.Drawing.Size(50, 25)
        Me.Delete_Position.TabIndex = 15
        Me.Delete_Position.Text = "Delete"
        Me.Delete_Position.UseVisualStyleBackColor = True
        '
        'Add_Position
        '
        Me.Add_Position.Location = New System.Drawing.Point(189, 322)
        Me.Add_Position.Name = "Add_Position"
        Me.Add_Position.Size = New System.Drawing.Size(50, 25)
        Me.Add_Position.TabIndex = 14
        Me.Add_Position.Text = "Add"
        Me.Add_Position.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 325)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "ID:"
        '
        'Text_IDPosition
        '
        Me.Text_IDPosition.Location = New System.Drawing.Point(40, 322)
        Me.Text_IDPosition.Name = "Text_IDPosition"
        Me.Text_IDPosition.Size = New System.Drawing.Size(134, 20)
        Me.Text_IDPosition.TabIndex = 19
        '
        'Text_Position
        '
        Me.Text_Position.Location = New System.Drawing.Point(66, 354)
        Me.Text_Position.Name = "Text_Position"
        Me.Text_Position.Size = New System.Drawing.Size(108, 20)
        Me.Text_Position.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 357)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Position:"
        '
        'Back_PositionMngmnt
        '
        Me.Back_PositionMngmnt.Location = New System.Drawing.Point(12, 5)
        Me.Back_PositionMngmnt.Name = "Back_PositionMngmnt"
        Me.Back_PositionMngmnt.Size = New System.Drawing.Size(45, 25)
        Me.Back_PositionMngmnt.TabIndex = 22
        Me.Back_PositionMngmnt.Text = "<<"
        Me.Back_PositionMngmnt.UseVisualStyleBackColor = True
        '
        'PositionManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(358, 391)
        Me.Controls.Add(Me.Back_PositionMngmnt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Text_Position)
        Me.Controls.Add(Me.Text_IDPosition)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Clear_Position)
        Me.Controls.Add(Me.Edit_Position)
        Me.Controls.Add(Me.Delete_Position)
        Me.Controls.Add(Me.Add_Position)
        Me.Controls.Add(Me.Dgv_Position)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "PositionManagementForm"
        Me.Text = "Position Management Form"
        CType(Me.Dgv_Position, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Dgv_Position As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Clear_Position As System.Windows.Forms.Button
    Friend WithEvents Edit_Position As System.Windows.Forms.Button
    Friend WithEvents Delete_Position As System.Windows.Forms.Button
    Friend WithEvents Add_Position As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Text_IDPosition As System.Windows.Forms.TextBox
    Friend WithEvents Text_Position As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Back_PositionMngmnt As System.Windows.Forms.Button
End Class
