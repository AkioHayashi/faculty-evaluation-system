﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SubjectForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SubjectForm))
        Me.Back_SubjectMngmnt = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Text_Sub = New System.Windows.Forms.TextBox()
        Me.Text_IDSubject = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Clear_Subject = New System.Windows.Forms.Button()
        Me.Edit_Subject = New System.Windows.Forms.Button()
        Me.Delete_Subject = New System.Windows.Forms.Button()
        Me.Add_Subject = New System.Windows.Forms.Button()
        Me.Dgv_Subject = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.Dgv_Subject, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Back_SubjectMngmnt
        '
        Me.Back_SubjectMngmnt.Location = New System.Drawing.Point(32, 9)
        Me.Back_SubjectMngmnt.Name = "Back_SubjectMngmnt"
        Me.Back_SubjectMngmnt.Size = New System.Drawing.Size(45, 25)
        Me.Back_SubjectMngmnt.TabIndex = 32
        Me.Back_SubjectMngmnt.Text = "<<"
        Me.Back_SubjectMngmnt.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(33, 361)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Subject:"
        '
        'Text_Sub
        '
        Me.Text_Sub.Location = New System.Drawing.Point(86, 358)
        Me.Text_Sub.Name = "Text_Sub"
        Me.Text_Sub.Size = New System.Drawing.Size(108, 20)
        Me.Text_Sub.TabIndex = 30
        '
        'Text_IDSubject
        '
        Me.Text_IDSubject.Location = New System.Drawing.Point(60, 326)
        Me.Text_IDSubject.Name = "Text_IDSubject"
        Me.Text_IDSubject.Size = New System.Drawing.Size(134, 20)
        Me.Text_IDSubject.TabIndex = 29
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 329)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 13)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "ID:"
        '
        'Clear_Subject
        '
        Me.Clear_Subject.Location = New System.Drawing.Point(209, 361)
        Me.Clear_Subject.Name = "Clear_Subject"
        Me.Clear_Subject.Size = New System.Drawing.Size(50, 25)
        Me.Clear_Subject.TabIndex = 27
        Me.Clear_Subject.Text = "Clear"
        Me.Clear_Subject.UseVisualStyleBackColor = True
        '
        'Edit_Subject
        '
        Me.Edit_Subject.Location = New System.Drawing.Point(265, 326)
        Me.Edit_Subject.Name = "Edit_Subject"
        Me.Edit_Subject.Size = New System.Drawing.Size(50, 25)
        Me.Edit_Subject.TabIndex = 26
        Me.Edit_Subject.Text = "Edit"
        Me.Edit_Subject.UseVisualStyleBackColor = True
        '
        'Delete_Subject
        '
        Me.Delete_Subject.Location = New System.Drawing.Point(265, 361)
        Me.Delete_Subject.Name = "Delete_Subject"
        Me.Delete_Subject.Size = New System.Drawing.Size(50, 25)
        Me.Delete_Subject.TabIndex = 25
        Me.Delete_Subject.Text = "Delete"
        Me.Delete_Subject.UseVisualStyleBackColor = True
        '
        'Add_Subject
        '
        Me.Add_Subject.Location = New System.Drawing.Point(209, 326)
        Me.Add_Subject.Name = "Add_Subject"
        Me.Add_Subject.Size = New System.Drawing.Size(50, 25)
        Me.Add_Subject.TabIndex = 24
        Me.Add_Subject.Text = "Add"
        Me.Add_Subject.UseVisualStyleBackColor = True
        '
        'Dgv_Subject
        '
        Me.Dgv_Subject.AllowUserToAddRows = False
        Me.Dgv_Subject.AllowUserToDeleteRows = False
        Me.Dgv_Subject.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.Dgv_Subject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Dgv_Subject.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.Dgv_Subject.Location = New System.Drawing.Point(32, 40)
        Me.Dgv_Subject.Name = "Dgv_Subject"
        Me.Dgv_Subject.ReadOnly = True
        Me.Dgv_Subject.Size = New System.Drawing.Size(330, 271)
        Me.Dgv_Subject.TabIndex = 23
        '
        'Column1
        '
        Me.Column1.HeaderText = "ID"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Column2
        '
        Me.Column2.HeaderText = "Subject"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Column2.Width = 287
        '
        'SubjectForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(389, 412)
        Me.Controls.Add(Me.Back_SubjectMngmnt)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Text_Sub)
        Me.Controls.Add(Me.Text_IDSubject)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Clear_Subject)
        Me.Controls.Add(Me.Edit_Subject)
        Me.Controls.Add(Me.Delete_Subject)
        Me.Controls.Add(Me.Add_Subject)
        Me.Controls.Add(Me.Dgv_Subject)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SubjectForm"
        Me.Text = "SubjectForm"
        CType(Me.Dgv_Subject, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Back_SubjectMngmnt As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Text_Sub As System.Windows.Forms.TextBox
    Friend WithEvents Text_IDSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Clear_Subject As System.Windows.Forms.Button
    Friend WithEvents Edit_Subject As System.Windows.Forms.Button
    Friend WithEvents Delete_Subject As System.Windows.Forms.Button
    Friend WithEvents Add_Subject As System.Windows.Forms.Button
    Friend WithEvents Dgv_Subject As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
