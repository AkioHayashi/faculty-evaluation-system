﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StudentManagementForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(StudentManagementForm))
        Me.dgvStudent = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtEvdate = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFN = New System.Windows.Forms.TextBox()
        Me.txtLN = New System.Windows.Forms.TextBox()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Clear_Position = New System.Windows.Forms.Button()
        Me.Edit_Position = New System.Windows.Forms.Button()
        Me.Delete_Position = New System.Windows.Forms.Button()
        Me.Add_Position = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        CType(Me.dgvStudent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvStudent
        '
        Me.dgvStudent.AllowUserToAddRows = False
        Me.dgvStudent.AllowUserToDeleteRows = False
        Me.dgvStudent.BackgroundColor = System.Drawing.SystemColors.ControlLight
        Me.dgvStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStudent.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.dgvStudent.Location = New System.Drawing.Point(12, 51)
        Me.dgvStudent.Name = "dgvStudent"
        Me.dgvStudent.ReadOnly = True
        Me.dgvStudent.Size = New System.Drawing.Size(644, 239)
        Me.dgvStudent.TabIndex = 7
        '
        'ID
        '
        Me.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ID.Visible = False
        Me.ID.Width = 43
        '
        'Column1
        '
        Me.Column1.HeaderText = "Last Name"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 150
        '
        'Column2
        '
        Me.Column2.HeaderText = "First Name"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 150
        '
        'Column3
        '
        Me.Column3.HeaderText = "Evaluation Date"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Column3.Width = 150
        '
        'Column4
        '
        Me.Column4.HeaderText = "ID Number"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Column4.Width = 150
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(137, 379)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 13)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "ID:"
        '
        'txtEvdate
        '
        Me.txtEvdate.Location = New System.Drawing.Point(229, 350)
        Me.txtEvdate.Name = "txtEvdate"
        Me.txtEvdate.Size = New System.Drawing.Size(148, 20)
        Me.txtEvdate.TabIndex = 20
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(137, 326)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "First Name:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(137, 357)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(86, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Evalutaion Date:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(137, 300)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Last Name:"
        '
        'txtFN
        '
        Me.txtFN.Location = New System.Drawing.Point(204, 323)
        Me.txtFN.Name = "txtFN"
        Me.txtFN.Size = New System.Drawing.Size(173, 20)
        Me.txtFN.TabIndex = 24
        '
        'txtLN
        '
        Me.txtLN.Location = New System.Drawing.Point(204, 296)
        Me.txtLN.Name = "txtLN"
        Me.txtLN.Size = New System.Drawing.Size(173, 20)
        Me.txtLN.TabIndex = 25
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(164, 376)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(213, 20)
        Me.txtID.TabIndex = 26
        '
        'Clear_Position
        '
        Me.Clear_Position.Location = New System.Drawing.Point(383, 350)
        Me.Clear_Position.Name = "Clear_Position"
        Me.Clear_Position.Size = New System.Drawing.Size(68, 46)
        Me.Clear_Position.TabIndex = 30
        Me.Clear_Position.Text = "Clear"
        Me.Clear_Position.UseVisualStyleBackColor = True
        '
        'Edit_Position
        '
        Me.Edit_Position.Location = New System.Drawing.Point(457, 296)
        Me.Edit_Position.Name = "Edit_Position"
        Me.Edit_Position.Size = New System.Drawing.Size(71, 49)
        Me.Edit_Position.TabIndex = 29
        Me.Edit_Position.Text = "Edit"
        Me.Edit_Position.UseVisualStyleBackColor = True
        '
        'Delete_Position
        '
        Me.Delete_Position.Location = New System.Drawing.Point(457, 350)
        Me.Delete_Position.Name = "Delete_Position"
        Me.Delete_Position.Size = New System.Drawing.Size(71, 46)
        Me.Delete_Position.TabIndex = 28
        Me.Delete_Position.Text = "Delete"
        Me.Delete_Position.UseVisualStyleBackColor = True
        '
        'Add_Position
        '
        Me.Add_Position.Location = New System.Drawing.Point(383, 296)
        Me.Add_Position.Name = "Add_Position"
        Me.Add_Position.Size = New System.Drawing.Size(68, 49)
        Me.Add_Position.TabIndex = 27
        Me.Add_Position.Text = "Add"
        Me.Add_Position.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(12, 24)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(52, 21)
        Me.btnBack.TabIndex = 31
        Me.btnBack.Text = "<<"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'StudentManagementForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ClientSize = New System.Drawing.Size(670, 417)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.Clear_Position)
        Me.Controls.Add(Me.Edit_Position)
        Me.Controls.Add(Me.Delete_Position)
        Me.Controls.Add(Me.Add_Position)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.txtLN)
        Me.Controls.Add(Me.txtFN)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtEvdate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvStudent)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "StudentManagementForm"
        Me.Text = "StudentManagementForm"
        CType(Me.dgvStudent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvStudent As System.Windows.Forms.DataGridView
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEvdate As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtFN As System.Windows.Forms.TextBox
    Friend WithEvents txtLN As System.Windows.Forms.TextBox
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents Clear_Position As System.Windows.Forms.Button
    Friend WithEvents Edit_Position As System.Windows.Forms.Button
    Friend WithEvents Delete_Position As System.Windows.Forms.Button
    Friend WithEvents Add_Position As System.Windows.Forms.Button
    Friend WithEvents btnBack As System.Windows.Forms.Button
End Class
