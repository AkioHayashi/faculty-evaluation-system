﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FacultyPeerEvaluationForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.RadioButton21 = New System.Windows.Forms.RadioButton()
        Me.RadioButton22 = New System.Windows.Forms.RadioButton()
        Me.RadioButton23 = New System.Windows.Forms.RadioButton()
        Me.RadioButton24 = New System.Windows.Forms.RadioButton()
        Me.RadioButton25 = New System.Windows.Forms.RadioButton()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.RadioButton16 = New System.Windows.Forms.RadioButton()
        Me.RadioButton17 = New System.Windows.Forms.RadioButton()
        Me.RadioButton18 = New System.Windows.Forms.RadioButton()
        Me.RadioButton19 = New System.Windows.Forms.RadioButton()
        Me.RadioButton20 = New System.Windows.Forms.RadioButton()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.RadioButton26 = New System.Windows.Forms.RadioButton()
        Me.RadioButton27 = New System.Windows.Forms.RadioButton()
        Me.RadioButton28 = New System.Windows.Forms.RadioButton()
        Me.RadioButton29 = New System.Windows.Forms.RadioButton()
        Me.RadioButton30 = New System.Windows.Forms.RadioButton()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.RadioButton11 = New System.Windows.Forms.RadioButton()
        Me.RadioButton12 = New System.Windows.Forms.RadioButton()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.RadioButton31 = New System.Windows.Forms.RadioButton()
        Me.RadioButton32 = New System.Windows.Forms.RadioButton()
        Me.RadioButton33 = New System.Windows.Forms.RadioButton()
        Me.RadioButton34 = New System.Windows.Forms.RadioButton()
        Me.RadioButton35 = New System.Windows.Forms.RadioButton()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.RadioButton36 = New System.Windows.Forms.RadioButton()
        Me.RadioButton37 = New System.Windows.Forms.RadioButton()
        Me.RadioButton38 = New System.Windows.Forms.RadioButton()
        Me.RadioButton39 = New System.Windows.Forms.RadioButton()
        Me.RadioButton40 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButton6 = New System.Windows.Forms.RadioButton()
        Me.RadioButton7 = New System.Windows.Forms.RadioButton()
        Me.RadioButton8 = New System.Windows.Forms.RadioButton()
        Me.RadioButton9 = New System.Windows.Forms.RadioButton()
        Me.RadioButton10 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton5 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(322, 401)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 21
        Me.Button1.Text = "Submit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.RadioButton21)
        Me.GroupBox5.Controls.Add(Me.RadioButton22)
        Me.GroupBox5.Controls.Add(Me.RadioButton23)
        Me.GroupBox5.Controls.Add(Me.RadioButton24)
        Me.GroupBox5.Controls.Add(Me.RadioButton25)
        Me.GroupBox5.Location = New System.Drawing.Point(368, 58)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox5.TabIndex = 20
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Parameter 5"
        '
        'RadioButton21
        '
        Me.RadioButton21.AutoSize = True
        Me.RadioButton21.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton21.Name = "RadioButton21"
        Me.RadioButton21.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton21.TabIndex = 0
        Me.RadioButton21.TabStop = True
        Me.RadioButton21.Text = "1"
        Me.RadioButton21.UseVisualStyleBackColor = True
        '
        'RadioButton22
        '
        Me.RadioButton22.AutoSize = True
        Me.RadioButton22.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton22.Name = "RadioButton22"
        Me.RadioButton22.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton22.TabIndex = 4
        Me.RadioButton22.TabStop = True
        Me.RadioButton22.Text = "5"
        Me.RadioButton22.UseVisualStyleBackColor = True
        '
        'RadioButton23
        '
        Me.RadioButton23.AutoSize = True
        Me.RadioButton23.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton23.Name = "RadioButton23"
        Me.RadioButton23.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton23.TabIndex = 1
        Me.RadioButton23.TabStop = True
        Me.RadioButton23.Text = "2"
        Me.RadioButton23.UseVisualStyleBackColor = True
        '
        'RadioButton24
        '
        Me.RadioButton24.AutoSize = True
        Me.RadioButton24.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton24.Name = "RadioButton24"
        Me.RadioButton24.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton24.TabIndex = 3
        Me.RadioButton24.TabStop = True
        Me.RadioButton24.Text = "4"
        Me.RadioButton24.UseVisualStyleBackColor = True
        '
        'RadioButton25
        '
        Me.RadioButton25.AutoSize = True
        Me.RadioButton25.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton25.Name = "RadioButton25"
        Me.RadioButton25.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton25.TabIndex = 2
        Me.RadioButton25.TabStop = True
        Me.RadioButton25.Text = "3"
        Me.RadioButton25.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.RadioButton16)
        Me.GroupBox4.Controls.Add(Me.RadioButton17)
        Me.GroupBox4.Controls.Add(Me.RadioButton18)
        Me.GroupBox4.Controls.Add(Me.RadioButton19)
        Me.GroupBox4.Controls.Add(Me.RadioButton20)
        Me.GroupBox4.Location = New System.Drawing.Point(216, 219)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox4.TabIndex = 16
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Parameter 4"
        '
        'RadioButton16
        '
        Me.RadioButton16.AutoSize = True
        Me.RadioButton16.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton16.Name = "RadioButton16"
        Me.RadioButton16.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton16.TabIndex = 0
        Me.RadioButton16.TabStop = True
        Me.RadioButton16.Text = "1"
        Me.RadioButton16.UseVisualStyleBackColor = True
        '
        'RadioButton17
        '
        Me.RadioButton17.AutoSize = True
        Me.RadioButton17.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton17.Name = "RadioButton17"
        Me.RadioButton17.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton17.TabIndex = 4
        Me.RadioButton17.TabStop = True
        Me.RadioButton17.Text = "5"
        Me.RadioButton17.UseVisualStyleBackColor = True
        '
        'RadioButton18
        '
        Me.RadioButton18.AutoSize = True
        Me.RadioButton18.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton18.Name = "RadioButton18"
        Me.RadioButton18.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton18.TabIndex = 1
        Me.RadioButton18.TabStop = True
        Me.RadioButton18.Text = "2"
        Me.RadioButton18.UseVisualStyleBackColor = True
        '
        'RadioButton19
        '
        Me.RadioButton19.AutoSize = True
        Me.RadioButton19.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton19.Name = "RadioButton19"
        Me.RadioButton19.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton19.TabIndex = 3
        Me.RadioButton19.TabStop = True
        Me.RadioButton19.Text = "4"
        Me.RadioButton19.UseVisualStyleBackColor = True
        '
        'RadioButton20
        '
        Me.RadioButton20.AutoSize = True
        Me.RadioButton20.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton20.Name = "RadioButton20"
        Me.RadioButton20.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton20.TabIndex = 2
        Me.RadioButton20.TabStop = True
        Me.RadioButton20.Text = "3"
        Me.RadioButton20.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.RadioButton26)
        Me.GroupBox6.Controls.Add(Me.RadioButton27)
        Me.GroupBox6.Controls.Add(Me.RadioButton28)
        Me.GroupBox6.Controls.Add(Me.RadioButton29)
        Me.GroupBox6.Controls.Add(Me.RadioButton30)
        Me.GroupBox6.Location = New System.Drawing.Point(368, 219)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox6.TabIndex = 19
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Parameter 6"
        '
        'RadioButton26
        '
        Me.RadioButton26.AutoSize = True
        Me.RadioButton26.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton26.Name = "RadioButton26"
        Me.RadioButton26.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton26.TabIndex = 0
        Me.RadioButton26.TabStop = True
        Me.RadioButton26.Text = "1"
        Me.RadioButton26.UseVisualStyleBackColor = True
        '
        'RadioButton27
        '
        Me.RadioButton27.AutoSize = True
        Me.RadioButton27.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton27.Name = "RadioButton27"
        Me.RadioButton27.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton27.TabIndex = 4
        Me.RadioButton27.TabStop = True
        Me.RadioButton27.Text = "5"
        Me.RadioButton27.UseVisualStyleBackColor = True
        '
        'RadioButton28
        '
        Me.RadioButton28.AutoSize = True
        Me.RadioButton28.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton28.Name = "RadioButton28"
        Me.RadioButton28.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton28.TabIndex = 1
        Me.RadioButton28.TabStop = True
        Me.RadioButton28.Text = "2"
        Me.RadioButton28.UseVisualStyleBackColor = True
        '
        'RadioButton29
        '
        Me.RadioButton29.AutoSize = True
        Me.RadioButton29.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton29.Name = "RadioButton29"
        Me.RadioButton29.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton29.TabIndex = 3
        Me.RadioButton29.TabStop = True
        Me.RadioButton29.Text = "4"
        Me.RadioButton29.UseVisualStyleBackColor = True
        '
        'RadioButton30
        '
        Me.RadioButton30.AutoSize = True
        Me.RadioButton30.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton30.Name = "RadioButton30"
        Me.RadioButton30.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton30.TabIndex = 2
        Me.RadioButton30.TabStop = True
        Me.RadioButton30.Text = "3"
        Me.RadioButton30.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.RadioButton11)
        Me.GroupBox3.Controls.Add(Me.RadioButton12)
        Me.GroupBox3.Controls.Add(Me.RadioButton13)
        Me.GroupBox3.Controls.Add(Me.RadioButton14)
        Me.GroupBox3.Controls.Add(Me.RadioButton15)
        Me.GroupBox3.Location = New System.Drawing.Point(216, 58)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Parameter 3"
        '
        'RadioButton11
        '
        Me.RadioButton11.AutoSize = True
        Me.RadioButton11.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton11.Name = "RadioButton11"
        Me.RadioButton11.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton11.TabIndex = 0
        Me.RadioButton11.TabStop = True
        Me.RadioButton11.Text = "1"
        Me.RadioButton11.UseVisualStyleBackColor = True
        '
        'RadioButton12
        '
        Me.RadioButton12.AutoSize = True
        Me.RadioButton12.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton12.Name = "RadioButton12"
        Me.RadioButton12.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton12.TabIndex = 4
        Me.RadioButton12.TabStop = True
        Me.RadioButton12.Text = "5"
        Me.RadioButton12.UseVisualStyleBackColor = True
        '
        'RadioButton13
        '
        Me.RadioButton13.AutoSize = True
        Me.RadioButton13.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton13.TabIndex = 1
        Me.RadioButton13.TabStop = True
        Me.RadioButton13.Text = "2"
        Me.RadioButton13.UseVisualStyleBackColor = True
        '
        'RadioButton14
        '
        Me.RadioButton14.AutoSize = True
        Me.RadioButton14.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton14.TabIndex = 3
        Me.RadioButton14.TabStop = True
        Me.RadioButton14.Text = "4"
        Me.RadioButton14.UseVisualStyleBackColor = True
        '
        'RadioButton15
        '
        Me.RadioButton15.AutoSize = True
        Me.RadioButton15.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton15.TabIndex = 2
        Me.RadioButton15.TabStop = True
        Me.RadioButton15.Text = "3"
        Me.RadioButton15.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.RadioButton31)
        Me.GroupBox7.Controls.Add(Me.RadioButton32)
        Me.GroupBox7.Controls.Add(Me.RadioButton33)
        Me.GroupBox7.Controls.Add(Me.RadioButton34)
        Me.GroupBox7.Controls.Add(Me.RadioButton35)
        Me.GroupBox7.Location = New System.Drawing.Point(511, 58)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox7.TabIndex = 18
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Parameter 7"
        '
        'RadioButton31
        '
        Me.RadioButton31.AutoSize = True
        Me.RadioButton31.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton31.Name = "RadioButton31"
        Me.RadioButton31.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton31.TabIndex = 0
        Me.RadioButton31.TabStop = True
        Me.RadioButton31.Text = "1"
        Me.RadioButton31.UseVisualStyleBackColor = True
        '
        'RadioButton32
        '
        Me.RadioButton32.AutoSize = True
        Me.RadioButton32.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton32.Name = "RadioButton32"
        Me.RadioButton32.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton32.TabIndex = 4
        Me.RadioButton32.TabStop = True
        Me.RadioButton32.Text = "5"
        Me.RadioButton32.UseVisualStyleBackColor = True
        '
        'RadioButton33
        '
        Me.RadioButton33.AutoSize = True
        Me.RadioButton33.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton33.Name = "RadioButton33"
        Me.RadioButton33.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton33.TabIndex = 1
        Me.RadioButton33.TabStop = True
        Me.RadioButton33.Text = "2"
        Me.RadioButton33.UseVisualStyleBackColor = True
        '
        'RadioButton34
        '
        Me.RadioButton34.AutoSize = True
        Me.RadioButton34.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton34.Name = "RadioButton34"
        Me.RadioButton34.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton34.TabIndex = 3
        Me.RadioButton34.TabStop = True
        Me.RadioButton34.Text = "4"
        Me.RadioButton34.UseVisualStyleBackColor = True
        '
        'RadioButton35
        '
        Me.RadioButton35.AutoSize = True
        Me.RadioButton35.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton35.Name = "RadioButton35"
        Me.RadioButton35.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton35.TabIndex = 2
        Me.RadioButton35.TabStop = True
        Me.RadioButton35.Text = "3"
        Me.RadioButton35.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.RadioButton36)
        Me.GroupBox8.Controls.Add(Me.RadioButton37)
        Me.GroupBox8.Controls.Add(Me.RadioButton38)
        Me.GroupBox8.Controls.Add(Me.RadioButton39)
        Me.GroupBox8.Controls.Add(Me.RadioButton40)
        Me.GroupBox8.Location = New System.Drawing.Point(517, 219)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox8.TabIndex = 17
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Parameter 8"
        '
        'RadioButton36
        '
        Me.RadioButton36.AutoSize = True
        Me.RadioButton36.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton36.Name = "RadioButton36"
        Me.RadioButton36.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton36.TabIndex = 0
        Me.RadioButton36.TabStop = True
        Me.RadioButton36.Text = "1"
        Me.RadioButton36.UseVisualStyleBackColor = True
        '
        'RadioButton37
        '
        Me.RadioButton37.AutoSize = True
        Me.RadioButton37.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton37.Name = "RadioButton37"
        Me.RadioButton37.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton37.TabIndex = 4
        Me.RadioButton37.TabStop = True
        Me.RadioButton37.Text = "5"
        Me.RadioButton37.UseVisualStyleBackColor = True
        '
        'RadioButton38
        '
        Me.RadioButton38.AutoSize = True
        Me.RadioButton38.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton38.Name = "RadioButton38"
        Me.RadioButton38.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton38.TabIndex = 1
        Me.RadioButton38.TabStop = True
        Me.RadioButton38.Text = "2"
        Me.RadioButton38.UseVisualStyleBackColor = True
        '
        'RadioButton39
        '
        Me.RadioButton39.AutoSize = True
        Me.RadioButton39.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton39.Name = "RadioButton39"
        Me.RadioButton39.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton39.TabIndex = 3
        Me.RadioButton39.TabStop = True
        Me.RadioButton39.Text = "4"
        Me.RadioButton39.UseVisualStyleBackColor = True
        '
        'RadioButton40
        '
        Me.RadioButton40.AutoSize = True
        Me.RadioButton40.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton40.Name = "RadioButton40"
        Me.RadioButton40.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton40.TabIndex = 2
        Me.RadioButton40.TabStop = True
        Me.RadioButton40.Text = "3"
        Me.RadioButton40.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton6)
        Me.GroupBox2.Controls.Add(Me.RadioButton7)
        Me.GroupBox2.Controls.Add(Me.RadioButton8)
        Me.GroupBox2.Controls.Add(Me.RadioButton9)
        Me.GroupBox2.Controls.Add(Me.RadioButton10)
        Me.GroupBox2.Location = New System.Drawing.Point(67, 219)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Parameter 2"
        '
        'RadioButton6
        '
        Me.RadioButton6.AutoSize = True
        Me.RadioButton6.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton6.Name = "RadioButton6"
        Me.RadioButton6.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton6.TabIndex = 0
        Me.RadioButton6.TabStop = True
        Me.RadioButton6.Text = "1"
        Me.RadioButton6.UseVisualStyleBackColor = True
        '
        'RadioButton7
        '
        Me.RadioButton7.AutoSize = True
        Me.RadioButton7.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton7.Name = "RadioButton7"
        Me.RadioButton7.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton7.TabIndex = 4
        Me.RadioButton7.TabStop = True
        Me.RadioButton7.Text = "5"
        Me.RadioButton7.UseVisualStyleBackColor = True
        '
        'RadioButton8
        '
        Me.RadioButton8.AutoSize = True
        Me.RadioButton8.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton8.Name = "RadioButton8"
        Me.RadioButton8.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton8.TabIndex = 1
        Me.RadioButton8.TabStop = True
        Me.RadioButton8.Text = "2"
        Me.RadioButton8.UseVisualStyleBackColor = True
        '
        'RadioButton9
        '
        Me.RadioButton9.AutoSize = True
        Me.RadioButton9.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton9.Name = "RadioButton9"
        Me.RadioButton9.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton9.TabIndex = 3
        Me.RadioButton9.TabStop = True
        Me.RadioButton9.Text = "4"
        Me.RadioButton9.UseVisualStyleBackColor = True
        '
        'RadioButton10
        '
        Me.RadioButton10.AutoSize = True
        Me.RadioButton10.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton10.Name = "RadioButton10"
        Me.RadioButton10.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton10.TabIndex = 2
        Me.RadioButton10.TabStop = True
        Me.RadioButton10.Text = "3"
        Me.RadioButton10.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.RadioButton5)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton4)
        Me.GroupBox1.Controls.Add(Me.RadioButton3)
        Me.GroupBox1.Location = New System.Drawing.Point(67, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(126, 155)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Parameter 1"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(20, 29)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "1"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton5
        '
        Me.RadioButton5.AutoSize = True
        Me.RadioButton5.Location = New System.Drawing.Point(20, 125)
        Me.RadioButton5.Name = "RadioButton5"
        Me.RadioButton5.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton5.TabIndex = 4
        Me.RadioButton5.TabStop = True
        Me.RadioButton5.Text = "5"
        Me.RadioButton5.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(20, 53)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "2"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(20, 101)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton4.TabIndex = 3
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "4"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(20, 77)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(31, 17)
        Me.RadioButton3.TabIndex = 2
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "3"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'FacultyPeerEvaluationForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(710, 482)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FacultyPeerEvaluationForm"
        Me.Text = "FacultyPeerEvaluationForm"
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton21 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton22 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton23 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton24 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton25 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton16 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton17 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton18 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton19 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton20 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton26 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton27 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton28 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton29 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton30 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton11 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton12 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton13 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton14 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton15 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton31 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton32 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton33 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton34 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton35 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton36 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton37 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton38 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton39 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton40 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton6 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton7 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton8 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton9 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton10 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton5 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
End Class
