﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.View_FacultyResult = New System.Windows.Forms.Button()
        Me.Logout_Admin = New System.Windows.Forms.Button()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.OvalShape1 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.OvalShape2 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.Faculty_Management = New System.Windows.Forms.Button()
        Me.Position_Management = New System.Windows.Forms.Button()
        Me.Account_Management = New System.Windows.Forms.Button()
        Me.Student_Management = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'View_FacultyResult
        '
        Me.View_FacultyResult.Location = New System.Drawing.Point(236, 328)
        Me.View_FacultyResult.Name = "View_FacultyResult"
        Me.View_FacultyResult.Size = New System.Drawing.Size(151, 68)
        Me.View_FacultyResult.TabIndex = 4
        Me.View_FacultyResult.Text = "View Faculty Result"
        Me.View_FacultyResult.UseVisualStyleBackColor = True
        '
        'Logout_Admin
        '
        Me.Logout_Admin.Location = New System.Drawing.Point(530, 132)
        Me.Logout_Admin.Name = "Logout_Admin"
        Me.Logout_Admin.Size = New System.Drawing.Size(75, 23)
        Me.Logout_Admin.TabIndex = 5
        Me.Logout_Admin.Text = "Logout"
        Me.Logout_Admin.UseVisualStyleBackColor = True
        '
        'LineShape1
        '
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 0
        Me.LineShape1.X2 = 627
        Me.LineShape1.Y1 = 120
        Me.LineShape1.Y2 = 120
        '
        'OvalShape1
        '
        Me.OvalShape1.Location = New System.Drawing.Point(20, 20)
        Me.OvalShape1.Name = "OvalShape1"
        Me.OvalShape1.Size = New System.Drawing.Size(75, 66)
        '
        'OvalShape2
        '
        Me.OvalShape2.Location = New System.Drawing.Point(530, 20)
        Me.OvalShape2.Name = "OvalShape2"
        Me.OvalShape2.Size = New System.Drawing.Size(75, 66)
        '
        'RectangleShape1
        '
        Me.RectangleShape1.Location = New System.Drawing.Point(150, 25)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(323, 56)
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape1, Me.OvalShape2, Me.OvalShape1, Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(624, 442)
        Me.ShapeContainer1.TabIndex = 6
        Me.ShapeContainer1.TabStop = False
        '
        'Faculty_Management
        '
        Me.Faculty_Management.Location = New System.Drawing.Point(323, 254)
        Me.Faculty_Management.Name = "Faculty_Management"
        Me.Faculty_Management.Size = New System.Drawing.Size(151, 68)
        Me.Faculty_Management.TabIndex = 7
        Me.Faculty_Management.Text = "Faculty Management"
        Me.Faculty_Management.UseVisualStyleBackColor = True
        '
        'Position_Management
        '
        Me.Position_Management.Location = New System.Drawing.Point(150, 254)
        Me.Position_Management.Name = "Position_Management"
        Me.Position_Management.Size = New System.Drawing.Size(151, 68)
        Me.Position_Management.TabIndex = 8
        Me.Position_Management.Text = "Position Management"
        Me.Position_Management.UseVisualStyleBackColor = True
        '
        'Account_Management
        '
        Me.Account_Management.Location = New System.Drawing.Point(323, 180)
        Me.Account_Management.Name = "Account_Management"
        Me.Account_Management.Size = New System.Drawing.Size(151, 68)
        Me.Account_Management.TabIndex = 9
        Me.Account_Management.Text = "Account Management"
        Me.Account_Management.UseVisualStyleBackColor = True
        '
        'Student_Management
        '
        Me.Student_Management.Location = New System.Drawing.Point(150, 180)
        Me.Student_Management.Name = "Student_Management"
        Me.Student_Management.Size = New System.Drawing.Size(151, 68)
        Me.Student_Management.TabIndex = 10
        Me.Student_Management.Text = "Student Management"
        Me.Student_Management.UseVisualStyleBackColor = True
        '
        'AdminForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(624, 442)
        Me.Controls.Add(Me.Student_Management)
        Me.Controls.Add(Me.Account_Management)
        Me.Controls.Add(Me.Position_Management)
        Me.Controls.Add(Me.Faculty_Management)
        Me.Controls.Add(Me.Logout_Admin)
        Me.Controls.Add(Me.View_FacultyResult)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AdminForm"
        Me.Text = "AdminForm"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents View_FacultyResult As System.Windows.Forms.Button
    Friend WithEvents Logout_Admin As System.Windows.Forms.Button
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents OvalShape1 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents OvalShape2 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents Faculty_Management As System.Windows.Forms.Button
    Friend WithEvents Position_Management As System.Windows.Forms.Button
    Friend WithEvents Account_Management As System.Windows.Forms.Button
    Friend WithEvents Student_Management As System.Windows.Forms.Button
End Class
